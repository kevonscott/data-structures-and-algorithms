class Graph:
    def __init__(self, adj_list,) -> None:
        self.graph = adj_list
        self.parent = {k: None for k in adj_list.keys()} # track parent of each node
        self.cost = {k: float('inf') for k in adj_list.keys()}  # track cost to each node
        self.processed = []  # Track nodes already processed

    def _next_node(self):
        """Find the next unvisited node with minimum cost"""
        min_node = None
        min_cost = float('inf')
        for node, cost in self.cost.items():
            if (cost < min_cost) and (node not in self.processed):
                min_cost = cost
                min_node = node
        return min_node, min_cost

    def dijkstra(self, src, dest):
        # Add source node to processed
        self.processed.append(src)
        self.cost[src] = 0

        # Add all of src neighbors to self.parent and self.cost since we already know them
        for node, cost in self.graph[src]:
            self.parent[node] = src
            self.cost[node] = cost
        
        next_node, next_cost = self._next_node()
        while next_node:
            for neighbor, neighbor_cost in self.graph[next_node]:
                new_cost = next_cost + neighbor_cost
                if not self.cost.get(neighbor):  # node does not point to any other nodes
                    self.processed.append(next_node)
                    next_node, next_cost = self._next_node()
                    continue
                if new_cost < self.cost[neighbor]:
                    self.cost[neighbor] = new_cost
                    self.parent[neighbor] = next_node
            self.processed.append(next_node)
            next_node, next_cost = self._next_node()
        
        order = []
        next_item = dest
        order.append(next_item)

        for _ in self.parent:
            next_item = self.parent[next_item]
            if next_item:
                order.append(next_item)
            else:
                break
        dest_cost = self.cost[dest]

        if dest_cost != float('inf'):
            print(
                f'Path exists from {src} to {dest}.\n'
                f'Cost of path is: {dest_cost}\n'
                f'Path: {"==>".join(order[::-1])}'
            )
        else:
            print(f'No path exists from {src} to {dest}.')


if __name__=='__main__':
    adj_list = {
        'A': [('B',4), ('C',2)],
        'B': [('C',5), ('D',10)],
        'C': [('E',3)],
        'D': [('F',11)],
        'E': [('D',4)],
    }
    g = Graph(adj_list=adj_list)
    g.dijkstra('A', 'D')